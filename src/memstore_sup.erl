%% Copyright (c) 2017-present. Benoit Chesneau
%%
%% This source code is licensed under the MIT license found in the
%% LICENSE file in the root directory of this source tree.
%%
%%%-------------------------------------------------------------------
%% @doc memstore top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(memstore_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}

%% TODO: validate flags
init([]) ->
  Spec = #{
    id => db,
    start => {memstore, start_link, []},
    restart => transient,
    shutdown => infinity,
    type => supervisor,
    modules => [memstore]
  },
  
  SupFlags = #{
    strategy => simple_one_for_one,
    intensity => 3,
    period => 3600
  },
  {ok, {SupFlags, [Spec]} }.


%%====================================================================
%% Internal functions
%%====================================================================
