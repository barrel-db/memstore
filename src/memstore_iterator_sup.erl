%% Copyright (c) 2017-present. Benoit Chesneau
%%
%% This source code is licensed under the MIT license found in the
%% LICENSE file in the root directory of this source tree.

-module(memstore_iterator_sup).
-author("benoitc").
-behaviour(supervisor).

-export([start_link/1]).

-export([init/1]).

start_link(Name) ->
  supervisor:start_link({local, Name}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}

%% TODO: validate flags
init([]) ->
  Spec = #{
    id => db,
    start => {memstore_iterator, start_link, []},
    restart => transient,
    shutdown => 2000,
    type => worker,
    modules => [memstore]
  },
  
  SupFlags = #{
    strategy => simple_one_for_one,
    intensity => 1,
    period => 10
  },
  {ok, {SupFlags, [Spec]} }.
